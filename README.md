# Mini projet sur les inférences avec JeuxDeMots


## Getting started

Le notebook JeuxDeMots.ipynb se trouve dans le répertoire "Colab Projet Langage Naturel".

Lancez le notebook JeuxDeMots.ipynb sur Jupyter Notebook ou avec Google Colab si vous n'avez pas Jupyter d'installé.

**Si vous utilisez Colab :**
Je vous conseille de créer un répertoire sur Google Drive dans lequel vous mettez le notebook et l'executez, comme ça les différents fichier CSV crée seront dans le même répertoire.

**Si vous utilisez Jupyter Notebook :**
Les fichiers CSV doivent être dans le même répertoire que le notebook lors de l'exécution.

Si jamais je vous laisserai plusieurs fichiers CSV dans le répertoire du Git pour avoir déjà de quoi tester certains termes.

## Exécution

Executez la première cellule si vous n'avez pas NumPy ou Pandas d'installé sur votre machine.

Executez chaque cellule jusqu'à la fonction search(). Vous pouvez sauter les cellules 6 et 7 qui sont là seulement si vous passez par Google Drive.

L'appel à search() dans la cellule 50 lance le programme et vous demande les termes que vous voulez utiliser.

Le programme se termine parfois sans réponses. C'est parce que le nombres d'inférences possible est inférieur au nombre d'inférences demandées, voire la cellule en question où la solution est expliquée.

Les seules relations prisent en charge sont : r_is_a | r_has_part | r_patient | r_carac | r_agent-1

N'hésitez pas si vous avez un problème ou des questions : florian.thepaut@etu.umontpellier.fr



Sujet du projet : https://docs.google.com/document/d/1njrZm9WEVkAM7zTXvnNMov-fR4KfbUn1DQ5V1AeRkR4/edit#heading=h.pcvi355pg5z8